<?php

/**
 * Implements hook_process_html().
 *
 * Unescape values stored in the scripts variable.
 */
function json_escaped_string_process_html(&$variables) {
  $variables['scripts'] = json_escaped_string::unescape($variables['scripts']);
}

/**
 * Class JSON Escaped String
 *
 * Drupal uses php's json_encode() to convert php arrays and objects into
 * javascript's object literal syntax. The JSON specification however is much
 * more narrow than the full breadth of object literals; in particular, values
 * are more or less limited to strings and numbers. This disallows expressions,
 * anonymous functions, and even variable references.
 *
 * This class avoids the issue by giving json_encode() a unique identifier
 * which can then be replaced by the original value later in execution.
 *
 * This class was written to allow callback functions to be passed as part of an
 * initialization map for external js libraries. For example, to establish 
 * settings for colorbox to use custom onOpen and onClosed callbacks:
 * @code
 *   function mymodule_colorbox_settings_alter(&$settings) {
 *     $settings['onClosed'] = new json_escaped_string('Drupal.myModule.open');
 *     $settings['onOpen'] = new json_escaped_string('
 *       function () { alert("even more popups"); }
 *     ');
 *   }
 * @endcode
 */
class json_escaped_string {
  // only use private variables so that json_encode will not output them
  private $value = '';

  public function __construct($value = '') {
    $this->value = $value;
    // save the hash as a public property so that json_encode() will include it
    $this->{__CLASS__} = spl_object_hash($this);
    self::register($this);
  }

  public function __toString() {
    return $this->value;
  }

  static function register($obj = NULL) {
    $hashes = &drupal_static(__CLASS__.'::'.__FUNCTION__, array());
    if(!isset($obj)) {
      return $hashes;
    }
    $search = drupal_json_encode($obj);
    $hashes[$search] = $obj;
  }

  static function unescape($string = '') {
    $hashes = self::register();
    $search = array_keys($hashes);
    $replace = array_values($hashes);
    return str_replace($search, $replace, $string);
  }
}
